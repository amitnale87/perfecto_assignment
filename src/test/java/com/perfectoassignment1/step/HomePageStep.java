package com.perfectoassignment1.step;

import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.util.List;

import org.hamcrest.Matchers;

import com.component.utils.ComponentsPageStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class HomePageStep extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "text.homepagetitle.homepage")
	private QAFWebElement textHomepagetitleHomepage;
	@FindBy(locator = "text.allowlink.homepage")
	private QAFWebElement textAllowlinkHomepage;
	@FindBy(locator = "link.signout.HomePage")
	private QAFWebElement linkSignoutHomepage;
	@FindBy(locator = "list.location.HomePage")
	private List<ComponentsPageStep> listLocationHomepage;
	
	public static String secondLinkName;

	/* By default Overloaded Method */
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	/* Getters Methods */
	public QAFWebElement getTextHomepagetitleHomepage() {
		return textHomepagetitleHomepage;
	}

	public QAFWebElement getTextAllowlinkHomepage() {
		return textAllowlinkHomepage;
	}

	public QAFWebElement getLinkSignoutHomepage() {
		return linkSignoutHomepage;
	}

	public List<ComponentsPageStep> getListLocationHomepage() {
		return listLocationHomepage;
	}
	
	public String getsecondLinkName() {
		return secondLinkName;
	}

	
	/* Business Methods */
	@QAFTestStep(description = "Verify User Logged in on Home Page")
	public void verifyHomePage() {
		try 
		{
			textAllowlinkHomepage.waitForPresent();
			textAllowlinkHomepage.click();
			
			
		} catch(Exception e) 
		{
			Reporter.log("Allow Link wasn't appeared this time");
		}
		
		 
		
		Reporter.log("The Current Title is " + textHomepagetitleHomepage.getText());
		//System.out.println("The Current Title is " + textHomepagetitleHomepage.getText());
		
		Validator.verifyThat(textHomepagetitleHomepage.getText(),Matchers.containsString("Jobs"));
		verifyPresent("link.signout.HomePage");

	}

	@QAFTestStep(description = "User Selects second option from the available list")
	public void clickOnSecondItem() {
		Reporter.log("List Details: ");
		//System.out.println("List Details: ");
		waitForPageToLoad();
		Reporter.log("Total Number of item in the list: "+listLocationHomepage.size());
		//System.out.println("Total Number of item in the list: "+listLocationHomepage.size());
		for(int i=0; i<listLocationHomepage.size();i++)
		{
			//System.out.println(listLocationHomepage.get(i).getText());
			Reporter.log("Main Route Name: "+listLocationHomepage.get(i).getLinkMainroutenameHomepagecomponent().getText());
			Reporter.log("Sub Route Name: "+listLocationHomepage.get(i).getLinkSubrouteHomepagecomponent().getText());
			Reporter.log("Distance in Miles: "+listLocationHomepage.get(i).getLinkDistanceHomepagecomponent().getText());
		}
		Reporter.log("Clicking on second element");
		//listLocationHomepage.get(1).click(); this will work for QAFWebElement i.e. without components
		
		/*Storing second link information in Static Variable*/
		secondLinkName=listLocationHomepage.get(1).getLinkMainroutenameHomepagecomponent().getText();
		listLocationHomepage.get(1).getLinkMainroutenameHomepagecomponent().click();
		
		
	}
	
	@QAFTestStep(description = "Click on Sign Out Button")
	public void clickOnSignOut() {
		Reporter.log("Clicking on Log Out link");
		linkSignoutHomepage.click();
	}

}
