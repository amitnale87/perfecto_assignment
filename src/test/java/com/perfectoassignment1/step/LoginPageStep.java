package com.perfectoassignment1.step;

import com.perfecto.utils.PerfectoUtilities;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class LoginPageStep extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "text.emailid.loginPage")
	private QAFWebElement textEmailidLoginPage;
	@FindBy(locator = "text.password.LoginPage")
	private QAFWebElement textPasswordLoginPage;
	@FindBy(locator = "btn.signIn.LoginPage")
	private QAFWebElement btnSignInLoginPage;
	@FindBy(locator = "btn.return.LoginPage")
	private QAFWebElement btnReturnLoginpage;

	/* Getters Methods */
	public QAFWebElement getTextEmailidLoginPage() {
		return textEmailidLoginPage;
	}

	public QAFWebElement getTextPasswordLoginPage() {
		return textPasswordLoginPage;
	}

	public QAFWebElement getBtnSignInLoginPage() {
		return btnSignInLoginPage;
	}
	
	public QAFWebElement getBtnReturnLoginpage() {
		return btnReturnLoginpage;
	}

	/* By default Overloaded Method */
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {

	}

	/* Business Methods */
	@QAFTestStep(description = "User Launches an Application on desired device")
	public void launchApplication() {
		String deviceType=ConfigurationManager.getBundle().getString("platformName");
		if(deviceType.equalsIgnoreCase("Andriod"))
		{
			PerfectoUtilities.installApplication(pageProps.getPropertyValue("driver.capabilities.appPath"), driver);
			PerfectoUtilities.openApplication("com.infostretch.sourceapp", driver);
		}
		else
		{
			Reporter.log("As this is iOS device we can't install app through script,"
					+ "so autolauch property is set to true");
		}



	}

	@QAFTestStep(description = "I login with {0} and {1}")
	public void loginWithCredentials(String userName, String password) {
		textEmailidLoginPage.sendKeys(userName);

		/*In case of iOS iPad need to hide keybord*/
		try {
			btnReturnLoginpage.waitForPresent();
			if(btnReturnLoginpage.isPresent())
			{
				btnReturnLoginpage.click();
			}

		}catch(Exception e)
		{
			Reporter.log("This device may not be iPad Device thats why 'Return' button doesn't appeared");
		}


		textPasswordLoginPage.sendKeys(password);

		/*In case of iOS iPad need to hide keybord*/
		try {
			btnReturnLoginpage.waitForPresent();
			if(btnReturnLoginpage.isPresent())
			{
				btnReturnLoginpage.click();
			}
		}catch(Exception e)
		{
			Reporter.log("This device may not be iPad Device thats why 'Return' button doesn't appeared");
		}

		btnSignInLoginPage.click();

	}



}
