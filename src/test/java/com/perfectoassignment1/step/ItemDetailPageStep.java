package com.perfectoassignment1.step;

import org.hamcrest.Matchers;

import com.perfecto.utils.PerfectoUtilities;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ItemDetailPageStep extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.back.ItemDetailPage")
	private QAFWebElement btnBackItemDetailPage;
	@FindBy(locator = "text.maincategory.itemdetailpage")
	private QAFWebElement textMaincategoryItemdetailpage;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getBtnBackItemDetailPage() {
		return btnBackItemDetailPage;
	}

	public QAFWebElement getTextMaincategoryItemdetailpage() {
		return textMaincategoryItemdetailpage;
	}

	/*Business Logic Methods*/
	@QAFTestStep(description = "User come back to home page")
	public void clickBackButton() {
		btnBackItemDetailPage.click();
	}


	@QAFTestStep(description = "User moves to detail page verify item info against list page")
	public void validateProductInfo() {
		//waitForPageToLoad();
		
		Reporter.log("From List Page "+HomePageStep.secondLinkName);
		
		PerfectoUtilities.swapApplication(driver);
		textMaincategoryItemdetailpage.waitForVisible(60000);
		
		Reporter.log("From Detail page: "+textMaincategoryItemdetailpage.getText());
		Validator.verifyThat(HomePageStep.secondLinkName, Matchers.containsString(textMaincategoryItemdetailpage.getText()));
	}

}
