package com.component.utils;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ComponentsPageStep extends QAFWebComponent {

	public ComponentsPageStep(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "link.mainroutename.homepagecomponent")
	private QAFWebElement linkMainroutenameHomepagecomponent;
	@FindBy(locator = "link.subroute.homepagecomponent")
	private QAFWebElement linkSubrouteHomepagecomponent;
	@FindBy(locator = "link.distance.homepagecomponent")
	private QAFWebElement linkDistanceHomepagecomponent;

	/* By default Overloaded Method */
	

	/* Getters Methods */
	public QAFWebElement getLinkMainroutenameHomepagecomponent() {
		return linkMainroutenameHomepagecomponent;
	}

	public QAFWebElement getLinkSubrouteHomepagecomponent() {
		return linkSubrouteHomepagecomponent;
	}

	public QAFWebElement getLinkDistanceHomepagecomponent() {
		return linkDistanceHomepagecomponent;
	}

}
