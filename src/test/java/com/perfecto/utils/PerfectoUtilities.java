package com.perfecto.utils;

import java.util.HashMap;
import java.util.Map;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;

public class PerfectoUtilities extends WebDriverTestCase{
	
	/*Perfecto Commands*/
	
	public  static void installApplication(String fileAddress, QAFExtendedWebDriver driver)
	{
		Map<String, Object> params = new HashMap<>();
		  
		//params.put("file", "PUBLIC:Public Media\\AmitAndriodApp\\Resigned_FieldService.apk");
		//params.put("file", ConfigurationManager.getBundle().getPropertyValue("driver.capabilities.appPath"));
		params.put("file", fileAddress);
		params.put("instrument", "noinstrument");
		driver.executeScript("mobile:application:install", params); 
		
	}
	
	
	public  static void openApplication(String appName, QAFExtendedWebDriver driver)
	
	{
		Map<String, Object> params = new HashMap<>();
		//params.put("name", "appName");
		params.put("identifier", appName);
		params.put("timeout", 60);
		driver.executeScript("mobile:application:open", params);
		
	}
		
	public  static void swapApplication(QAFExtendedWebDriver driver)
	{
		Map<String, Object> params = new HashMap<>();
		params.put("start", "50%,20%");
		params.put("end", "50%,60%");
		params.put("duration", "5");
		driver.executeScript("mobile:touch:swipe", params);
	}


	
	
}
